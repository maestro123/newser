package maestro.newser.ui;

import android.app.Activity;
import android.support.v4.app.Fragment;
import maestro.newser.Main;

/**
 * Created by Artyom on 16.09.2014.
 */
public class BaseFragment extends Fragment {

    public Main main;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        main = (Main) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        main = null;
    }
}
