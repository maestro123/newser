package maestro.newser.ui;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.dream.android.mim.MIM2;
import com.dream.android.mim.MIMInternetMaker;
import com.dream.android.mim.MIMRoundRectPostMaker;
import maestro.gfapi.model.Feed;
import maestro.gfapi.model.FeedEntry;
import maestro.gfapi.processors.XMLFeedProcessor;
import maestro.newser.R;
import maestro.newser.utils.BaseUpdateAdapter;
import maestro.requestor.Requestor;

/**
 * Created by Artyom on 16.09.2014.
 */
public class FeedEntryItemsFragment extends BaseFragment implements Requestor.ResponseListener {

    public static final String TAG = FeedEntryItemsFragment.class.getSimpleName();

    private static final String PARAM_ENTRY = "entry";

    public static final FeedEntryItemsFragment makeInstance(FeedEntry entry) {
        FeedEntryItemsFragment fragment = new FeedEntryItemsFragment();
        Bundle args = new Bundle(1);
        args.putParcelable(PARAM_ENTRY, entry);
        fragment.setArguments(args);
        return fragment;
    }

    private FeedEntry mEntry;
    private Requestor.Request mRequest;

    private ListView mList;
    private ProgressBar mProgress;
    private EntryAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEntry = getArguments().getParcelable(PARAM_ENTRY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.feed_entry_fragment_view, null);
        mList = (ListView) v.findViewById(R.id.list);
        mProgress = (ProgressBar) v.findViewById(R.id.progress);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mList.setAdapter(mAdapter = new EntryAdapter(getActivity()));
        if (mEntry.getFeedCount() == 0) {
            mProgress.setVisibility(View.VISIBLE);
            Log.e(TAG, "url= " + mEntry.getUrl());
            mRequest = new Requestor.Request(mEntry.getUrl())
                    .setPostProcessor(new XMLFeedProcessor()).setListener(this);
            mRequest.async();
            mProgress.animate().alpha(1f).alphaBy(0f).start();
        } else {

        }
    }

    @Override
    public void onResponse(Requestor.Request request, Object object) {
        if (object instanceof FeedEntry) {
            mEntry = (FeedEntry) object;
            mAdapter.update(mEntry.getFeeds());
            mProgress.animate().alpha(0f).start();
        }
    }

    @Override
    public void onError(Requestor.Request request, Requestor.REQUEST_ERROR error) {

    }

    private class EntryAdapter extends BaseUpdateAdapter<Feed> {

        private MIM2 mim;
        private MIMRoundRectPostMaker postMaker;

        public EntryAdapter(Context context) {
            super(context);
            final int size60dp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, getResources().getDisplayMetrics());
            mim = new MIM2(context).setDefaultSize(size60dp, size60dp).setMaker(new MIMInternetMaker(false));
            postMaker = new MIMRoundRectPostMaker(size60dp / 2);
        }

        @Override
        public View getView(int position, View v, ViewGroup parent) {
            Holder holder;
            if (v == null) {
                holder = new Holder();
                v = getLayoutInflater().inflate(R.layout.feed_item_view, null);
                holder.Title = (TextView) v.findViewById(R.id.title);
                holder.Date = (TextView) v.findViewById(R.id.date);
                holder.Content = (TextView) v.findViewById(R.id.content);
                holder.Image = (ImageView) v.findViewById(R.id.image);
                v.setTag(holder);
            } else {
                holder = (Holder) v.getTag();
            }
            Feed feed = getItem(position);
            holder.Title.setText(Html.fromHtml(feed.getTitle()));
            holder.Date.setText(feed.getPublishedDate());
            holder.Content.setText(feed.getContentSnippet() != null ? Html.fromHtml(feed.getContentSnippet())
                    : feed.getContent() != null ? Html.fromHtml(feed.getContent()) : null);
            if (feed.hasImages()) {
                holder.Image.setVisibility(View.VISIBLE);
                String key = feed.getImages()[feed.getImages().length - 1].Url;
                Log.e(TAG, "try load = " + key);
                mim.loadImage(new MIM2.ImageLoadObject(key, key).setPostMaker(postMaker), holder.Image);
            } else {
                holder.Image.setVisibility(View.GONE);
                mim.cancel(holder.Image, null);
            }
            return v;
        }

        class Holder {
            ImageView Image;
            TextView Title;
            TextView Content;
            TextView Date;
        }

    }

}
