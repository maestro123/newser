package maestro.newser;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.*;
import maestro.gfapi.GFApi;
import maestro.gfapi.model.FeedEntry;
import maestro.gfapi.model.FeedList;
import maestro.gfapi.processors.GFPostProcessor;
import maestro.newser.ui.FeedEntryItemsFragment;
import maestro.requestor.Requestor;

public class Main extends FragmentActivity {

    public static final String TAG = Main.class.getSimpleName();

    private ListView mListView;
    private ProgressBar mProgress;
    private EditText mEdit;
    private FeedAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mEdit = (EditText) findViewById(R.id.input);
        mListView = (ListView) findViewById(R.id.list);
        mProgress = (ProgressBar) findViewById(R.id.progress);

        mEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    String url = GFApi.buildFindRequest(mEdit.getText().toString());
                    Requestor.Request mRequest = new Requestor.Request(url)
                            .setPostProcessor(new GFPostProcessor(GFPostProcessor.KIND_FIND))
                            .setListener(new Requestor.ResponseListener() {
                                @Override
                                public void onResponse(Requestor.Request request, Object object) {
                                    if (object instanceof FeedList) {
                                        mAdapter.update((FeedList) object);
                                        mProgress.setVisibility(View.GONE);
                                    }
                                }

                                @Override
                                public void onError(Requestor.Request request, Requestor.REQUEST_ERROR error) {
                                    Log.e(TAG, error.name());
                                }
                            });
                    mProgress.setVisibility(View.VISIBLE);
                    mRequest.async();
                    return true;
                }
                return false;
            }
        });

        mListView.setAdapter(mAdapter = new FeedAdapter());
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame, FeedEntryItemsFragment.makeInstance(mAdapter.getItem(position)))
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    private class FeedAdapter extends BaseAdapter {

        FeedEntry[] mEntries;

        public FeedAdapter() {
        }

        public void update(FeedList feedList) {
            mEntries = feedList.getEntries();
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mEntries != null ? mEntries.length : 0;
        }

        @Override
        public FeedEntry getItem(int position) {
            return mEntries[position];
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView textView = new TextView(getApplicationContext());
            textView.setPadding(20, 20, 20, 20);
            textView.setText(Html.fromHtml(mEntries[position].getTitle()));
            textView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics()));
            return textView;
        }
    }

}
