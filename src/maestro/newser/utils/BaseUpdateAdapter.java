package maestro.newser.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;

/**
 * Created by Artyom on 16.09.2014.
 */
public abstract class BaseUpdateAdapter<T> extends BaseAdapter {

    private T[] items;
    private Context mContext;
    private LayoutInflater mInflater;

    public BaseUpdateAdapter(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items != null ? items.length : 0;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public T getItem(int position) {
        return items[position];
    }

    public T[] getItems() {
        return items;
    }

    public void update(T[] items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public Context getContext() {
        return mContext;
    }

    public LayoutInflater getLayoutInflater() {
        return mInflater;
    }

}
