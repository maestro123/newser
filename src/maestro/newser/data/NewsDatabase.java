package maestro.newser.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Artyom on 16.09.2014.
 */
public class NewsDatabase extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "news.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_FEED = "tbl_feed";
    public static final String TABLE_FAVOURITES = "tbl_favourite";

    public static final String KEY_ID = "id";
    public static final String KEY_TITLE = "title";
    public static final String KEY_ICON = "icon";
    public static final String KEY_LINK = "link";
    public static final String KEY_UPDATE_TIME = "upd_time";
    public static final String KEY_CONTENT = "content";

    public static final String CREATE_TABLE_FEED = "create table " + TABLE_FEED + "(" + KEY_ID +
            " integer primary key, " + KEY_TITLE + " text, " + KEY_ICON + " text, "
            + KEY_LINK + " text, " + KEY_UPDATE_TIME + " text, " + KEY_CONTENT + " text);";

    public static final String CREATE_TABLE_FAVOURITE = "create table " + TABLE_FAVOURITES + "("
            + KEY_ID + " integer primary key, " + KEY_LINK + " text);";

    public NewsDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_FEED);
        db.execSQL(CREATE_TABLE_FAVOURITE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
