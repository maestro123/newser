package maestro.newser.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Artyom on 16.09.2014.
 */
public class DataHelper {

    private static volatile DataHelper instance;

    public static synchronized DataHelper getInstance() {
        return instance != null ? instance : (instance = new DataHelper());
    }

    SQLiteDatabase mDatabase;

    DataHelper() {
    }

    public void create(Context context) {
        mDatabase = new NewsDatabase(context).getWritableDatabase();
    }

    public void destroy() {
        mDatabase.close();
    }

}
