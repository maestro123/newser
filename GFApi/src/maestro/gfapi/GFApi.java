package maestro.gfapi;

import java.net.URLEncoder;

/**
 * Created by Artyom on 16.09.2014.
 */
public class GFApi {

    public static final String VERSION = "?v=1.0";
    public static final String FIND_URL = "https://ajax.googleapis.com/ajax/services/feed/find" + VERSION;
    public static final String LOAD_URL = "https://ajax.googleapis.com/ajax/services/feed/load" + VERSION;
    public static final String PARAM_Q = "&q=";
    public static final String PARAM_LANG = "&hl=";

    public static String buildFindRequest(String query) {
        return buildFindRequest(query, null);
    }

    public static String buildFindRequest(String query, String lang) {
        StringBuilder builder = new StringBuilder(FIND_URL).append(PARAM_Q).append(URLEncoder.encode(query));
        if (lang != null) {
            builder.append(PARAM_LANG).append(lang);
        }
        return builder.toString();
    }

    public static String buildLoadRequest(String url) {
        return buildLoadRequest(url, null);
    }

    public static String buildLoadRequest(String url, String lang) {
        StringBuilder builder = new StringBuilder(LOAD_URL).append(PARAM_Q).append(url);
        if (lang != null) {
            builder.append(PARAM_LANG).append(lang);
        }
        return builder.toString();
    }

}
