package maestro.gfapi.processors;

import maestro.gfapi.model.GoogleFeedEntry;
import maestro.gfapi.model.GoogleFeedList;
import maestro.requestor.Requestor;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Artyom on 16.09.2014.
 */
public class GFPostProcessor implements Requestor.RequestPostProcessor {

    public static final byte KIND_FIND = 0x00;
    public static final byte KIND_LOAD = 0x01;

    public byte Kind;

    public GFPostProcessor(byte kind) {
        Kind = kind;
    }

    @Override
    public Object process(String response) {
        if (Kind == KIND_FIND) {
            try {
                return new GoogleFeedList(new JSONObject(response));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (Kind == KIND_LOAD) {
            try {
                return new GoogleFeedEntry(new JSONObject(response)
                        .getJSONObject("responseData").getJSONObject("feed"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return Requestor.REQUEST_ERROR.SERVER;
    }

}
