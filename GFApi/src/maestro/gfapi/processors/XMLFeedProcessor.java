package maestro.gfapi.processors;

import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.Html;
import android.util.Log;
import android.util.Xml;
import maestro.gfapi.model.FeedEntry;
import maestro.gfapi.model.FeedImage;
import maestro.gfapi.model.XMLFeed;
import maestro.gfapi.model.XMLFeedEntry;
import maestro.requestor.Requestor;
import org.xml.sax.XMLReader;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;

/**
 * Created by Artyom on 16.09.2014.
 */
public class XMLFeedProcessor implements Requestor.RequestPostProcessor {

    public static final String TAG = XMLFeedProcessor.class.getSimpleName();

    @Override
    public Object process(String response) {
        return parse(response);
    }

    private static final String KEY_CHANNEL = "(channel|feed)";
    private static final String KEY_ITEM = "(item|entry)";
    private static final String KEY_TITLE = "title";
    private static final String KEY_LINK = "link";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_CONTENT = "(content:encoded|content|summary)";
    private static final String KEY_DC_LANGUAGE = "dc:language";
    private static final String KEY_DATE = "(dc:date|pubDate|lastBuildDate|published|updated)";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_WIDTH = "width";
    private static final String KEY_HEIGHT = "height";
    private static final String KEY_URL = "url";
    private static final String KEY_CREATOR = "{dc:creator|author}";
    private static final String KEY_DC_SUBJECT = "dc:subject";
    private static final String KEY_ICON = "icon";
    private static final String KEY_MEDIA_CONTENT = "media:content";
    private static final String KEY_MEDIA_THUMBNAIL = "media:thumbnail";
    private static final String KEY_TYPE = "type";

    private XMLFeedEntry entry = null;
    private XMLFeed feed = null;
    private FeedImage image = null;
    private String text = null;

    public FeedEntry parse(String content) {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(new StringReader(content));
            int eventType = parser.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_DOCUMENT) {
                } else if (eventType == XmlPullParser.END_DOCUMENT) {
                } else if (eventType == XmlPullParser.START_TAG) {
                    String tag = parser.getName();
                    Log.e(TAG, "tag = " + tag);
                    if (tag.matches(KEY_CHANNEL)) {
                        entry = new XMLFeedEntry();
                    } else if (tag.matches(KEY_ITEM)) {
                        feed = new XMLFeed();
                    } else if (tag.equals(KEY_IMAGE)) {
                        image = new FeedImage();
                    } else if (tag.equals(KEY_MEDIA_CONTENT)) {
                        image = parseMediaImage(parser, true);
                        if (image != null && feed != null)
                            feed.addImage(image);
                    } else if (tag.equals(KEY_MEDIA_THUMBNAIL)) {
                        image = parseMediaImage(parser, false);
                        if (image != null && feed != null)
                            feed.addImage(image);
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    String tag = parser.getName();
                    if (tag.matches(KEY_ITEM)) {
                        entry.addFeed(feed);
                    } else if (tag.equals(KEY_TITLE)) {
                        if (feed == null) {
                            entry.setTitle(text);
                        } else {
                            feed.setTitle(text);
                        }
                    } else if (tag.equals(KEY_LINK)) {
                        if (feed == null) {
                            entry.setLink(text);
                        } else {
                            feed.setLink(text);
                        }
                    } else if (tag.equals(KEY_DESCRIPTION)) {
                        if (feed == null) {
                            entry.setContentSnippet(text);
                        } else {
                            feed.setContent(text);
                            if (feed.getContentSnippet() == null)
                                feed.setContentSnippet(text);
                            if (!feed.hasImages()) {
                                processImageFromContent(text);
                            }
                        }
                    } else if (tag.matches(KEY_CONTENT)) {
                        if (feed == null) {
                            if (entry.getContentSnippet() == null) {
                                entry.setContentSnippet(text);
                            }
                        } else {
                            feed.setContentSnippet(text);
                            if (!feed.hasImages()) {
                                processImageFromContent(text);
                            }
                        }
                    } else if (tag.equals(KEY_IMAGE)) {
                        if (feed != null)
                            feed.addImage(image);
                    } else if (tag.equals(KEY_URL)) {
                        if (image != null) {
                            image.setUrl(text);
                        }
                    } else if (tag.equals(KEY_WIDTH)) {
                        image.setWidth(Integer.valueOf(text));
                    } else if (tag.equals(KEY_HEIGHT)) {
                        image.setHeight(Integer.valueOf(text));
                    } else if (tag.matches(KEY_DATE)) {
                        if (feed == null) {
                            entry.setDate(text);
                        } else {
                            feed.setDate(text);
                        }
                    } else if (tag.equals(KEY_CREATOR)) {
                        feed.setCreator(text);
                    } else if (tag.equals(KEY_ICON)) {
                        entry.setIconUrl(text);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = parser.getText();
                }
                eventType = parser.next();
            }
            return entry;
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private FeedImage parseMediaImage(XmlPullParser parser, boolean check) {
        HashMap<String, String> attrs = parseAttributes(parser);
        if (attrs != null) {
            if ((attrs.containsKey(KEY_TYPE) && attrs.get(KEY_TYPE).contains("image")) || !check
                    || (attrs.size() == 1 && attrs.containsKey("url"))) {
                FeedImage image = new FeedImage();
                for (String key : attrs.keySet()) {
                    if (key.equals(KEY_TYPE))
                        image.setType(attrs.get(key));
                    else if (key.matches(KEY_URL))
                        image.setUrl(attrs.get(key));
                    else if (key.matches(KEY_WIDTH))
                        image.setWidth(Integer.valueOf(attrs.get(key)));
                    else if (key.matches(KEY_HEIGHT))
                        image.setHeight(Integer.valueOf(attrs.get(key)));
                }
                return image;
            }
        }
        return null;
    }

    public void processImageFromContent(String text) {
        Html.fromHtml(text, new Html.ImageGetter() {
            @Override
            public Drawable getDrawable(String s) {
                FeedImage image = new FeedImage();
                image.Url = fixImageUrl(s);
                feed.addImage(image);
                return null;
            }
        }, new Html.TagHandler() {
            @Override
            public void handleTag(boolean b, String s, Editable editable, XMLReader xmlReader) {

            }
        });
    }

    private HashMap<String, String> parseAttributes(XmlPullParser parser) {
        final int count = parser.getAttributeCount();
        HashMap<String, String> map = null;
        if (count > 0) {
            map = new HashMap<String, String>();
            for (int i = 0; i < count; i++) {
                map.put(parser.getAttributeName(i), parser.getAttributeValue(i));
            }
        }
        return map;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

    private String fixImageUrl(String url) {
        if (!url.startsWith("http")) {
            if (url.startsWith("//")) {
                url = new StringBuilder("http:").append(url).toString();
            }
        }
        return url;
    }

}
