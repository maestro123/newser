package maestro.gfapi.model;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Artyom on 16.09.2014.
 */
public class GoogleFeed implements Feed, Parcelable {

    private JSONObject jObj;
    private FeedImage[] mImages;

    public GoogleFeed(JSONObject _jObj) {
        jObj = _jObj;
        prepare();
    }

    public GoogleFeed(Parcel source) {
        try {
            jObj = new JSONObject(source.readString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void prepare() {
        try {
            JSONArray mediaGroups = jObj.optJSONArray("mediaGroups");
            if (mediaGroups != null && mediaGroups.length() > 0) {
                for (int i = 0; i < mediaGroups.length(); i++) {
                    JSONObject jsonObject = mediaGroups.getJSONObject(i);
                    if (jsonObject.has("contents")) {
                        JSONArray contentsArray = jsonObject.getJSONArray("contents");
                        final int contentsSize = contentsArray.length();
                        mImages = new FeedImage[contentsSize];
                        for (int j = 0; j < contentsSize; j++) {
                            mImages[j] = FeedImage.buildFromGoogle(contentsArray.getJSONObject(j));
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getTitle() {
        return jObj.optString("title");
    }

    @Override
    public String getLink() {
        return jObj.optString("link");
    }

    @Override
    public String getPublishedDate() {
        return jObj.optString("publishedDate");
    }

    @Override
    public String getContent() {
        return jObj.optString("content");
    }

    @Override
    public String getContentSnippet() {
        return jObj.optString("contentSnippet");
    }

    @Override
    public boolean hasImages() {
        return mImages != null && mImages.length > 0;
    }

    @Override
    public FeedImage[] getImages() {
        return mImages;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
