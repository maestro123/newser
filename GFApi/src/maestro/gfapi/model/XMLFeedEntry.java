package maestro.gfapi.model;

import android.os.Parcel;

import java.util.Arrays;

/**
 * Created by Artyom on 16.09.2014.
 */
public class XMLFeedEntry implements FeedEntry {

    private String url;
    private String title;
    private String contentSnippet;
    private String link;
    private String date;
    private String iconUrl;

    private Feed[] mFeeds;

    public XMLFeedEntry() {
    }

    public void addFeed(Feed feed) {
        if (mFeeds == null) {
            mFeeds = new Feed[0];
        }
        final int size = mFeeds.length;
        mFeeds = Arrays.copyOf(mFeeds, size + 1);
        mFeeds[size] = feed;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContentSnippet(String contentSnippet) {
        this.contentSnippet = contentSnippet;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getContentSnippet() {
        return contentSnippet;
    }

    @Override
    public String getLink() {
        return link;
    }

    public String getDate() {
        return date;
    }

    @Override
    public Feed[] getFeeds() {
        return mFeeds;
    }

    @Override
    public int getFeedCount() {
        return mFeeds != null ? mFeeds.length : 0;
    }

    @Override
    public Feed getFeedAtPosition(int position) {
        return mFeeds[position];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
