package maestro.gfapi.model;

/**
 * Created by Artyom on 16.09.2014.
 */
public interface Feed {

    public String getTitle();

    public String getLink();

    public String getPublishedDate();

    public String getContent();

    public String getContentSnippet();

    public boolean hasImages();

    public FeedImage[] getImages();

}
