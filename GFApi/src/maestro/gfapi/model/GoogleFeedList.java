package maestro.gfapi.model;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Artyom on 16.09.2014.
 */
public class GoogleFeedList implements Parcelable, FeedList {

    private JSONObject jObj;
    private GoogleFeedEntry[] mEntries;

    public GoogleFeedList(JSONObject _jObj) {
        this.jObj = _jObj;
        prepare();
    }

    public GoogleFeedList(Parcel source) {
        try {
            jObj = new JSONObject(source.readString());
            prepare();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void prepare() {
        try {
            JSONArray entries = jObj.getJSONObject("responseData").getJSONArray("entries");
            final int size = entries.length();
            mEntries = new GoogleFeedEntry[size];
            for (int i = 0; i < size; i++) {
                mEntries[i] = new GoogleFeedEntry(entries.getJSONObject(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public GoogleFeedEntry[] getEntries() {
        return mEntries;
    }

    public GoogleFeedEntry getEntryAtPosition(int position) {
        return mEntries[position];
    }

    public int getEntriesCount() {
        return mEntries != null ? mEntries.length : 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dest.readString());
    }

    public static final Creator<GoogleFeedList> CREATOR = new Creator<GoogleFeedList>() {
        @Override
        public GoogleFeedList createFromParcel(Parcel source) {
            return new GoogleFeedList(source);
        }

        @Override
        public GoogleFeedList[] newArray(int size) {
            return new GoogleFeedList[size];
        }
    };

}
