package maestro.gfapi.model;

/**
 * Created by Artyom on 16.09.2014.
 */
public interface FeedList {

    public FeedEntry[] getEntries();

    public FeedEntry getEntryAtPosition(int position);

    public int getEntriesCount();

}
