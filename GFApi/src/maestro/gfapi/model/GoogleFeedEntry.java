package maestro.gfapi.model;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Artyom on 16.09.2014.
 */
public class GoogleFeedEntry implements Parcelable, FeedEntry {

    private JSONObject jObj;
    private GoogleFeed[] mEntries;

    public GoogleFeedEntry(JSONObject _jObj) {
        jObj = _jObj;
        prepare();
    }

    public GoogleFeedEntry(Parcel source) {
        try {
            jObj = new JSONObject(source.readString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        prepare();
    }

    void prepare() {
        try {
            if (jObj.has("entries")) {
                JSONArray entriesArray = jObj.optJSONArray("entries");
                final int size = entriesArray.length();
                mEntries = new GoogleFeed[size];
                for (int i = 0; i < size; i++) {
                    mEntries[i] = new GoogleFeed(entriesArray.getJSONObject(i));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getUrl() {
        return jObj.optString("url");
    }

    public String getTitle() {
        return jObj.optString("title");
    }

    public String getContentSnippet() {
        return jObj.optString("contentSnippet");
    }

    public String getLink() {
        return jObj.optString("link");
    }

    @Override
    public Feed[] getFeeds() {
        return mEntries;
    }

    @Override
    public int getFeedCount() {
        return mEntries != null ? mEntries.length : 0;
    }

    @Override
    public Feed getFeedAtPosition(int position) {
        return mEntries[position];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(jObj.toString());
    }

    public static final Creator<GoogleFeedEntry> CREATOR = new Creator<GoogleFeedEntry>() {
        @Override
        public GoogleFeedEntry createFromParcel(Parcel source) {
            return new GoogleFeedEntry(source);
        }

        @Override
        public GoogleFeedEntry[] newArray(int size) {
            return new GoogleFeedEntry[size];
        }
    };

}
