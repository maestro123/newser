package maestro.gfapi.model;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;

/**
 * Created by Artyom on 16.09.2014.
 */
public class FeedImage implements Parcelable {

    public static final int NOT_SET = -1;

    public String Url;
    public String Type;
    public String Medium;
    public boolean IsDefault;
    public int Width;
    public int Height;

    public static FeedImage buildFromGoogle(JSONObject jObj) {
        FeedImage image = new FeedImage();
        image.Url = jObj.optString("url");
        image.Type = jObj.optString("type");
        image.Medium = jObj.optString("medium");
        image.IsDefault = jObj.optBoolean("isDefault");
        image.Width = jObj.optInt("width", NOT_SET);
        image.Height = jObj.optInt("height", NOT_SET);
        return image;
    }

    public FeedImage() {
    }

    public FeedImage(Parcel source) {
        Url = source.readString();
        Type = source.readString();
        Medium = source.readString();
        IsDefault = source.readByte() == 0x01;
        Width = source.readInt();
        Height = source.readInt();
    }

    public void setUrl(String url) {
        Url = url;
    }

    public void setType(String type) {
        Type = type;
    }

    public void setMedium(String medium) {
        Medium = medium;
    }

    public void setDefault(boolean isDefault) {
        IsDefault = isDefault;
    }

    public void setWidth(int width) {
        Width = width;
    }

    public void setHeight(int height) {
        Height = height;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Url);
        dest.writeString(Type);
        dest.writeString(Medium);
        dest.writeByte((byte) (IsDefault ? 0x01 : 0x00));
        dest.writeInt(Width);
        dest.writeInt(Height);
    }

    public static final Creator<FeedImage> CREATOR = new Creator<FeedImage>() {
        @Override
        public FeedImage createFromParcel(Parcel source) {
            return new FeedImage(source);
        }

        @Override
        public FeedImage[] newArray(int size) {
            return new FeedImage[size];
        }
    };

}
