package maestro.gfapi.model;

import java.util.Arrays;

/**
 * Created by Artyom on 17.09.2014.
 */
public class XMLFeed implements Feed {

    private String title;
    private String link;
    private String date;
    private String content;
    private String contentSnippet;
    private String creator;
    private String guid;
    private FeedImage[] mImages;
    private String[] category;

    public XMLFeed() {
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setContentSnippet(String contentSnippet) {
        this.contentSnippet = contentSnippet;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public void addImage(FeedImage image) {
        if (image != null) {
            if (mImages == null)
                mImages = new FeedImage[0];
            final int size = mImages.length;
            mImages = Arrays.copyOf(mImages, size + 1);
            mImages[size] = image;
        }
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getLink() {
        return link;
    }

    @Override
    public String getPublishedDate() {
        return date;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public String getContentSnippet() {
        return contentSnippet;
    }

    public String getCreator() {
        return creator;
    }

    @Override
    public boolean hasImages() {
        return mImages != null && mImages.length > 0;
    }

    @Override
    public FeedImage[] getImages() {
        return mImages;
    }
}
