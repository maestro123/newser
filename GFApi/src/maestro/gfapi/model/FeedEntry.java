package maestro.gfapi.model;

import android.os.Parcelable;

/**
 * Created by Artyom on 16.09.2014.
 */
public interface FeedEntry extends Parcelable {

    public String getUrl();

    public String getTitle();

    public String getContentSnippet();

    public String getLink();

    public Feed[] getFeeds();

    public int getFeedCount();

    public Feed getFeedAtPosition(int position);

}
